.module crt0

.globl  _main
.globl  _sys_set_rom
.globl  _sys_set_bios

.area   _HOME
.area   _CODE
.area   _INITIALIZER
.area   _GSINIT
.area   _GSFINAL

.area   _DATA
.area   _INITIALIZED
.area   _BSEG
.area   _BSS
.area   _HEAP

ENASLT = 0x0024
RSLREG = 0x0138
CLIKSW = 0xf3db
SLOT_SEL = 0xa8
SUBSLOT_SEL = 0xffff
EXPTBL = 0xfcc1

.area   _CODE
        ; ROM header
        .str "AB"
        .dw _main_init
        .db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

        ; entry point skipping the init code that is used
        ; when starting from cassette
        jp _cas_main

    ; for 48K rom support this routine sets page 0 to the rom slot
_sys_set_rom:
      di
      ld	 a, (romslot)
      ld	 h, #0x00
      jp   enaslt
      
    ; for 48K rom support this routine sets page 0 back to the bios
_sys_set_bios:
        ld	 a, (biosslot)
        ld	 h, #0x00
        call enaslt
        ei
        ret

		; set slot and subslot at target address
		; (from msx bios listing)
		; hl - target address
		; a  - slot : FxxxSSPP
		;             F  : expanded slot flag (if F=0, SS is ignored)
		;             SS : expanded subslot
		;             PP : primary slot
enaslt:
        call selprm               ; calculate bit pattern and mask code
        jp   m, eneslt            ; if expanded set secondary first
        in   a, (SLOT_SEL)
        and  c
        or   b
        out  (SLOT_SEL), a        ; set primary slot
        ret
eneslt:
        push hl
        call selexp               ; set secondary slot
        pop  hl
        jr   enaslt

		; calculate bit pattern and mask
selprm:
        di
        push af
        ld   a, h
        rlca
        rlca
        and  #3
        ld   e, a                 ; bank number
        ld   a,#0xC0
selprm1:
        rlca
        rlca
        dec e
        jp p, selprm1
        ld e, a                   ; mask pattern
        cpl
        ld c, a                   ; inverted mask pattern
        pop af
        push af
        and #3                    ; extract xxxxxxPP
        inc a
        ld b, a
        ld a, #0xAB
selprm2:
        add a, #0x55
        djnz selprm2
        ld d, a                   ; primary slot bit pattern
        and e
        ld b, a
        pop af
        and a                     ; if expanded slot set sign flag
        ret

		; set secondary slot
selexp:
        push af
        ld   a, d
        and  #0xC0                ; get slot number for bank 3
        ld   c, a
        pop  af
        push af
        ld   d, a
        in   a, (SLOT_SEL)
        ld   b, a
        and  #0x3F
        or   c
        out  (SLOT_SEL), a        ; set bank 3 to target slot
        ld   a, d
        rrca
        rrca
        and   #3
        ld   d, a
        ld   a, #0xAB             ; secondary slot to bit pattern
selexp1:
        add  a, #0x55
        dec  d
        jp   p,selexp1
        and  e
        ld   d, a
        ld   a, e
        cpl
        ld   h, a
        ld   a, (SUBSLOT_SEL)     ; read and update secondary slot register
        cpl
        ld   l,a
        and  h                    ; strip off old bits
        or   d                    ; add new bits
        ld   (SUBSLOT_SEL),a
        ld   a,b
        out  (SLOT_SEL),a        ; restore status
        pop  af
        and  #3
        ret

getslot:
        and	 #0x03
        ld	 c, a
        ld	 b, #0
        ld	 hl, #EXPTBL
        add	 hl,bc
        ld	 a, (hl)
        and	 #0x80
        jr	 z,exit
        or	 c
        ld	 c,a
        inc	 hl
        inc	 hl
        inc	 hl
        inc	 hl
        ld	 a, (hl)
        and	 #0x0C
exit:
		    or	 c
		    ret

_main_init::

        ; init the stack
        di
        ld sp, #0xf380
        ei

        ; setup memory
        ; ref: https://www.msx.org/forum/msx-talk/development/memory-pages-again

        call RSLREG
        call getslot
        ld  (biosslot),a
        
        call RSLREG
        rrca
        rrca
        call getslot
        ld   (romslot),a
        ld   h,#0x80
        call enaslt

_cas_main::
        ; disable key click sound
        xor a
        ld (CLIKSW), a

        call gsinit
        call _main

halt0:
        halt
        jr halt0

.area   _GSINIT
gsinit::
        ld bc, #l__INITIALIZER
        ld a, b
        or a, c
        jr Z, gsinit_next
        ld de, #s__INITIALIZED
        ld hl, #s__INITIALIZER
        ldir
gsinit_next:

.area   _GSFINAL
        ret

.area _DATA
romslot:	.ds 1
biosslot:	.ds 1
