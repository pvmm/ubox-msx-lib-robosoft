all: game

export PATH := $(PWD)/bin:$(PATH)

libs: lib ubox spman mplayer ap

game: bin libs
	make -C tools
	make -C game all

game-cas: bin libs
	make -C tools
	make -C game cas

test: game
	make -C tests test

openmsx: game
	make -C game openmsx

docs:
	make -C docs

ubox:
	make -C src/ubox

spman:
	make -C src/spman

mplayer:
	make -C src/mplayer

ap:
	make -C src/ap

bin/apultra: bin
	make -C tools ../bin/apultra

.PHONY: clean cleanall docs libs game game-cas
clean:
	make -C src/ubox clean
	make -C src/spman clean
	make -C src/mplayer clean
	make -C src/ap clean
	make -C game clean

lib:
ifeq ($(OS),Windows_NT)
	mkdir .\lib
else
	mkdir -p ./lib
endif

bin:
ifeq ($(OS),Windows_NT)
	mkdir .\bin
else
	mkdir -p ./bin
endif

cleanall: clean
ifeq ($(OS),Windows_NT)
	if exist .\bin rmdir /s/q .\bin
	if exist .\lib rmdir /s/q .\lib
else
	rm -rf ./bin ./lib
endif
	make -C docs clean
