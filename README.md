# ubox MSX lib

This is a set of libraries and tools to make MSX games using the C programming
language.

This version is a fork of the original ubox MSX lib (https://gitlab.com/reidrac/ubox-msx-lib) with a number of additional changes:
 - Build scripts adapted to work for Windows as well
 - Support for 48K ROM added
 - Converted to make use of new call convention introduced in SDCC 4.1.12
 - Updated to be compatible with SDCC 4.3
 - Added MSX2 screen 4 support with sprite mode 2
 - Added MSX2 palette setting

New function calls in ubox lib:
 - ubox_set_rom: set page 0 to rom (used for 48K ROM)
 - ubox_set_bios: set page 0 back to BIOS
 - ubox_get_mode: get current screen mode (internally also used by spman to determine if screen 2 or screen 4 is active)
 - ubox_fill_vm: fill part of VRAM with a certain value
 - ubox_get_msx_version: get MSX version (MSX1, 2, 2+ or Turbo-R)
 - ubox_set_palette: set r, g and b values of a color number

There are three main components:

  - **ubox**: thin wrapper around MSX 1 BIOS, focusing on Screen 2 and 32K
    cartridge ROMs.
  - **spman**: a simple sprite and pattern manager with flickering support.
  - **mplayer**: a wrapper around Arkos 2 AKM player, supporting music and
    priority based one channel sound effects.

There are also some utilities (e.g. compression), and tools to process data and
help you building your games.

The aim is making MSX games in C, without writing Z80 assembler or having a
deep knowledge of the system.

## Requirements

 - GNU Make (others may work)
 - a POSIX compatible environment

The [SDCC](http://sdcc.sourceforge.net/) compiler is also needed. Check the
following table for some hints on compatibility.

| Version | Compatible | Comments                                                |
| ---     | ---        | ---                                                     |
| 3.9.0   | no         | -                                                       |
| 4.0.0   | no         | -                                                       |
| 4.1.0   | no         | At least on Linux amd64 it generates broken Z80 code    |
| 4.2.0   | yes        | -                                                       |
| 4.3.0   | yes        | Not stable, errors in generated code                    |

If you want to build the example you will also need:

 - python 3
 - pillow
 - GCC (only the C compiler)
 - Disark, one of the tools distributed with Arkos Tracker 2 (download from
   [Arkos Tracker 2 website](http://www.julien-nevo.com/arkostracker/))

On Windows, build using:

 - latest version of Cygwin64 with package for gcc-core 10.2.0-1
 - SDCC 4.2.0 (preferred over 4.3.0 which is not stable)
 - python 3.9.1

If you want to build the docs you will also need:

 - pandoc
 - python 3
 - pygments
 - pandocfilters

## Building

To build the libraries run:

    make

After a successful build, the libraries should be in `./lib`.

The include files are ready to use in `./include`.

Add those directories in `SDCC`'s search path and you are ready to go.

Note: `make` is expected to be run from the root of the repo. The PATH env
variable will be set automatically.

### Running tests

There are tests that can be run with `make test`.

### Building the example

An example game is included with the libraries and it can be built with:

    make game

After a successful build, the game ROM should be in `./bin`.

#### CAS support

Although the focus is cartridge ROMs, CAS files (and audio) is still one of the
cheapest ways of loading homebrew games on a real MSX.

Optionally, is possible to generate a CAS file of the example game running:

    make game-cas

After a successful build, the game CAS should be in `./bin`.

The CAS support has some limitations:

 - It requires 32K of extra RAM.
 - The compressed ROM must be less than 24576 bytes.
 - The loader uses the BIOS, so it won't be fast.
 - Machines with disk must have it disabled (by pressing shift on boot), to
  have more memory available.

Despite these limitations, it is worth considering releasing your game in CAS
format as well as cartridge ROM.

### Building the docs

The documentation is available at
[usebox.net](https://www.usebox.net/jjm/ubox-msx-lib/), so this is optional.

To build the docs run:

    make docs

The reference in `HTML` format will be generated in `./docs`.

## Contributing

All contributions are welcome.

If you think you have found a bug, please submit a bug report providing some
information:

 - What was expected to happen
 - What actually happens
 - How to reproduce the issue

Some advice if you want to make a successful contribution:

 - Be cordial
 - Get early feedback, specially when working on a large contribution
 - Contributions always require a pull request and a review
 - Check the [TODO](TODO.md) for ideas!

### Formatting

This project uses [Black](https://github.com/psf/black) to format the Python
code, and this is checked in CI.

Before submitting any code to review, ensure you have installed Black 22.1.0 and run:

    black .

## Authors

This was mostly written by Juan J. Martinez during the development of
[Night Knight](https://www.usebox.net/jjm/night-knight/) and
[Uchūsen Gamma](https://www.usebox.net/jjm/uchusen-gamma/).

Other contributors:

 - Pedro de Medeiros
 - ToriHino
 - Your name here?

## Copying

This software is distributed under MIT license, unless stated otherwise.

See COPYING file.

**TL;DR**: the only condition is that you are required to preserve the copyright
and license notices. Licensed works, modifications, and larger works may be
distributed under different terms and without source code; this includes any game
made with the help of this software.

Credit is appreciated, but is not a legal requirement. For example: you can add
to the game's documentation a note like "This game uses ubox MSX lib".

There are some third party tools included here for convenience and are covered
by their own license or are public domain.

